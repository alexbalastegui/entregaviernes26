#include "ClientProxy.h"



ClientProxy::ClientProxy()
{
	pos = sf::Vector2f{ static_cast<float>(rand()%gameW), static_cast<float>(rand() % gameH) };  //TODO: cambiar por valores l�gicos
}


ClientProxy::~ClientProxy()
{
}

sf::Packet operator<<(sf::Packet & packet, const Movement & move)
{
	return packet << move.id << move.delta.x << move.delta.y << move.abs.x << move.abs.y;
}

sf::Packet operator>>(sf::Packet & packet, Movement & move)
{
	return packet >> move.id >> move.delta.x >> move.delta.y >> move.abs.x >> move.abs.y;
}
