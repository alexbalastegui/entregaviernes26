#include "ProtocolsUDP.h"

sf::Packet & operator<<(sf::Packet & packet, const S2CProtocol & code)
{
	return packet << static_cast<int>(code);
}

sf::Packet & operator>>(sf::Packet & packet, S2CProtocol & code)
{
	int aux;
	packet >> aux;
	code = static_cast<S2CProtocol>(aux);
	return packet;
}

sf::Packet & operator<<(sf::Packet & packet, const C2SProtocol & code)
{
	return packet << static_cast<int>(code);
}

sf::Packet & operator>>(sf::Packet & packet, C2SProtocol & code)
{
	int aux;
	packet >> aux;
	code = static_cast<C2SProtocol>(aux);
	return packet;
}
