#pragma once
#include <SFML\/Network.hpp>
#include <iostream>
enum class S2CProtocol {
	WELCOME,//Mensaje de confirmacion de conexion que contiene las posiciones iniciales y el numero de players.
	NEWPLAYER,//Cuando se conecta un jugador nuevo hay que enviar a todos los dem�s (n-1) el id y la posicion de ese player.
	OKMOVE,//Confirmacion de movimiento que se manda a todos los clientes (n) con el id del movimiento, el id del player y las posiciones x:y.
	PING,//Mensaje para comprobar que siguen conectados los (n) clientes.
	DISCONNECTED,//Mensaje que se env�a a todos los clientes (n) conforme se ha desconectado el cliente con id.
	DISCONNECTOK //Confirma la desconexi�n de un cliente
};
sf::Packet& operator <<(sf::Packet& packet, const S2CProtocol& code);
sf::Packet& operator >>(sf::Packet& packet, S2CProtocol& code);

enum class C2SProtocol {
	HELLO,//Mensaje de petici�n de conexion al servidor.
	ACK, //Mensaje de confirmaci�n de llegada por paquete (idPacket).
	MOVE, //Peticion al servidor para movernos, con el ID del mensaje, con nuestro IDPlayer y las posiciones a las que deseamos movernos.
	PONG,//Confirmaci�n de que seguimos conectados.
	ERROR, //Something went wrong
	OKNEWPLAYER, //Confirmaci�n de que hemos recibido al nuevo player
	DISCONNECT //Solicita desconectarse
};
sf::Packet& operator <<(sf::Packet& packet, const C2SProtocol& code);
sf::Packet& operator >>(sf::Packet& packet, C2SProtocol& code);