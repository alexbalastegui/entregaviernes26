#pragma once
#include <SFML/Network.hpp>
#include "constants.h"

struct Movement {
	int id;
	sf::Vector2f delta;
	sf::Vector2f abs;
};

sf::Packet operator <<(sf::Packet &packet, const Movement &move);
sf::Packet operator >>(sf::Packet &packet, Movement &move);

class ClientProxy
{
	
public:
	ClientProxy();
	~ClientProxy();

	sf::IpAddress ip;
	unsigned short port;
	int id;
	sf::Vector2f pos;
	bool isCritical = false;
};

