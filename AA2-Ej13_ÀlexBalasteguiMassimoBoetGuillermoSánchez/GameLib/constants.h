#pragma once

//Movement in client
const float VEL = 1.1;
const float orderAcceptTime = 0.1;
const float playerInterpAlpha = 0.1f;

const float helloPollingTime = .1f;

//game
const int gameW = 400;
const int gameH = 300;
const int CIRCLERAD = 10;

//ServerQueue
const float timeToProcess = 1.f;

