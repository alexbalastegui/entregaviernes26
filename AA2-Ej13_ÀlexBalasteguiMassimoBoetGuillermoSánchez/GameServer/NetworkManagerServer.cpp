#include "NetworkManagerServer.h"



NetworkManagerServer::NetworkManagerServer()
{
	socket.bind(50000);
}


NetworkManagerServer::~NetworkManagerServer()
{
}

void NetworkManagerServer::MainLoop() {


	//check if we already know this client
	if (!processor.orders.empty()) {
		if (processor.canProcess()) {
			while (!processor.orders.empty()) {

				clients[processor.orders.front().first.id].pos.x += processor.orders.front().second.x;
				clients[processor.orders.front().first.id].pos.y += processor.orders.front().second.y;

				//std::cout << "X: " << processor.orders.front().second.x << " Y: " << processor.orders.front().second.y << " Size: " << processor.orders.size() << std::endl;

				/*if (clients[processor.orders.front().first.id].pos.x < 0)
					clients[processor.orders.front().first.id].pos.x += VEL;
				if (clients[processor.orders.front().first.id].pos.x > gameW-CIRCLERAD)
					clients[processor.orders.front().first.id].pos.x -= VEL;
				if (clients[processor.orders.front().first.id].pos.y < 0)
					clients[processor.orders.front().first.id].pos.y += VEL;
				if (clients[processor.orders.front().first.id].pos.y > gameH - CIRCLERAD)
					clients[processor.orders.front().first.id].pos.y -= VEL;*/

				sf::Packet pack;
				pack << S2CProtocol::OKMOVE << 
					processor.orders.front().first.id << 
					clients[processor.orders.front().first.id].pos.x << 
					clients[processor.orders.front().first.id].pos.y << 
					processor.orders.front().first.messageID;
				for (std::pair<int, ClientProxy> prok : clients) {
					socket.send(pack, prok.second.ip, prok.second.port);
				}
				processor.orders.pop_front();
			}
		}
	}
}

std::thread NetworkManagerServer::launchHearingThread()
{
	return std::thread([=] {HearingThread(&socket, &processor); });
}

void NetworkManagerServer::HearingThread(sf::UdpSocket* _socket, OrderProcessor* _processor)
{
	while (1) {
		sf::Packet pack;
		C2SProtocol code = C2SProtocol::ERROR;
		sf::IpAddress ip;
		unsigned short port;
		int id;
		if (_socket->receive(pack, ip, port) == sf::Socket::Status::Done) {
			pack >> id;
			pack >> code;


			//check if we already know this client
			bool canEnter = true;
			for (auto it = clients.begin(); it != clients.end(); ++it) {
				if (port == it->second.port && ip == it->second.ip) {
					canEnter = false;
				}
			}
			
			if (clients.find(id) == clients.end() && canEnter && id==-1) { //cliente no conocido
				
				int newPlayerID = getValidID();
				clients[newPlayerID].id = newPlayerID;
				clients[newPlayerID].ip = ip;
				clients[newPlayerID].port = port;

				std::cout << port << " has connected" << std::endl;
				pack.clear(); //When a new player enters we dont care what they say
				pack << S2CProtocol::WELCOME;

				pack << newPlayerID << clients[newPlayerID].pos.x << clients[newPlayerID].pos.y << static_cast<int>(clients.size() - 1); //El propio player
				pack << static_cast<int>(clients.size());
				for (std::pair<int, ClientProxy> prok : clients) { //Todos los clientes
					pack << prok.second.id << prok.second.pos.x << prok.second.pos.y;
				}
				socket.send(pack, clients[newPlayerID].ip, clients[newPlayerID].port); //Se env�a al nuevo player
				
				pack.clear();
				pack << S2CProtocol::NEWPLAYER;
				pack << newPlayerID << clients[newPlayerID].pos.x << clients[newPlayerID].pos.y; //el nuevo player
				//for (std::pair<int, ClientProxy> prok : clients) { //Todos los clientes
				//	//socket.send(pack, prok.second.ip, prok.second.port);
				//	prok.second.isCritical = true;
				//	std::thread proxyData = launchCriticalThread(&socket, pack, prok.second.port, prok.second.ip, &prok.second.isCritical);
				//	proxyData.detach();
				//}
				for (auto it = clients.begin(); it != clients.end(); ++it) {
					it->second.isCritical = true;
					std::thread proxyData = launchCriticalThread(&socket, pack, it->second.port, it->second.ip, &it->second.isCritical);
					proxyData.detach();
				}

			}
			else { //cliente conocido
				knownClient:
				switch (code) {
				case C2SProtocol::HELLO: {

					int playerID = -1;

					for (auto it = clients.begin(); it != clients.end(); ++it) {
						if (it->second.ip == ip && it->second.port == port) {
							playerID = it->second.id;
							break;
						}
					}

					//std::cout << port << "'s hello received" << std::endl;

					pack.clear(); //When a new player enters we dont care what they say
					pack << S2CProtocol::WELCOME;

					pack << playerID << clients[playerID].pos.x << clients[playerID].pos.y << static_cast<int>(clients.size() - 1); //El propio player
					pack << static_cast<int>(clients.size());
					for (std::pair<int, ClientProxy> prok : clients) { //Todos los clientes
						pack << prok.second.id << prok.second.pos.x << prok.second.pos.y;
					}
					socket.send(pack, clients[playerID].ip, clients[playerID].port); //Se env�a al nuevo player

					pack.clear();
				}
					break;
				case C2SProtocol::PONG:
					std::cout << port << " pong" << std::endl;
					break;
				case C2SProtocol::MOVE: {
					//process in main thread
					Movement move;
					pack >> move;
					_processor->addOrder(id, ip, port, code, move.id, move.delta.x, move.delta.y);
					//std::cout << "X: " << xDispl << " Y: " << yDispl << std::endl;
				}
					break;
				case C2SProtocol::OKNEWPLAYER:
					clients[id].isCritical = false;
					//std::cout << port << " :Received client confirmation" << std::endl;
					break;
				case C2SProtocol::DISCONNECT:
					pack.clear();
					pack << S2CProtocol::DISCONNECTOK;
					socket.send(pack, clients[id].ip, clients[id].port);
					clients[id].isCritical = NULL;
					clients.erase(id);
					std::cout << port << " can disconnect" << std::endl;
					break;
				}
			}
		}
	}
}

std::thread NetworkManagerServer::launchHeartbeat()
{
	return std::thread([=] {HeartbeatThread(&clients, &serverIsRunning); });
}

void NetworkManagerServer::criticalThread(sf::UdpSocket* _socket, sf::Packet pack, unsigned short port, sf::IpAddress ip, bool* _threadAlive)
{
	while (*_threadAlive && _threadAlive!=nullptr) {
		//std::cout << "Critical thread alive" << std::endl;
		if (criticalClock.getElapsedTime().asSeconds() > helloPollingTime) {
			std::cout << "Thread alive" << std::endl;
			//std::cout << "Critical thread: " << *_threadAlive << std::endl;
			socket.send(pack, ip, port);
			criticalClock.restart();
		}
	}
}

std::thread NetworkManagerServer::launchCriticalThread(sf::UdpSocket * _socket, sf::Packet pack, unsigned short port, sf::IpAddress ip, bool * _threadAlive)
{
	return std::thread([=] {criticalThread(_socket, pack, port, ip, _threadAlive); });
}

long NetworkManagerServer::getValidID()
{
	return clients.size();
}

sf::Packet NetworkManagerServer::newPlayerConnected(sf::Packet &pack,long id, sf::Vector2i pos, int ammountPlayers)
{
	return pack << id << pos.x << pos.y<< ammountPlayers;
}

void NetworkManagerServer::HeartbeatThread(std::map<int, ClientProxy>* _clients, bool* _serverIsRunning)
{
	sf::Clock clock;

	while (*_serverIsRunning) {
		if (clock.getElapsedTime().asSeconds() >= 5.f) {
			for (auto p : *_clients) {
				sf::Packet pack;
				pack << S2CProtocol::PING;
				socket.send(pack, p.second.ip, p.second.port);
				pack.clear();
			}
			clock.restart();
		}
	}

}
