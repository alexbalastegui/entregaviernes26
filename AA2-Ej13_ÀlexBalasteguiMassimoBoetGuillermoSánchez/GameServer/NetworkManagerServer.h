#pragma once
#include <map>
#include <SFML/Network.hpp>
#include <thread>
#include <iostream>
#include <ClientProxy.h>
#include <ProtocolsUDP.h>
#include <constants.h>
#include "OrderProcessor.h"

class NetworkManagerServer
{
public:
	std::map<int, ClientProxy> clients;//CONTENEDOR DE CLIENTES [ID]=INFOCLIENTE
	sf::UdpSocket socket;
	bool serverIsRunning = true;

	OrderProcessor processor;

	NetworkManagerServer();
	~NetworkManagerServer();

	std::thread launchHearingThread();
	void HearingThread(sf::UdpSocket* _socket, OrderProcessor* _processor);
	std::thread launchHeartbeat();

	sf::Clock criticalClock;
	bool threadAlive = false;
	void criticalThread(sf::UdpSocket* _socket, sf::Packet pack, unsigned short port, sf::IpAddress ip, bool* _threadAlive);
	std::thread launchCriticalThread(sf::UdpSocket* _socket, sf::Packet pack, unsigned short port, sf::IpAddress ip, bool* _threadAlive);

	long getValidID();
	sf::Packet newPlayerConnected(sf::Packet &pack, long id, sf::Vector2i pos, int ammountPlayers);
	void HeartbeatThread(std::map<int, ClientProxy>* _clients, bool* _serverIsRunning);
	void MainLoop();

};

