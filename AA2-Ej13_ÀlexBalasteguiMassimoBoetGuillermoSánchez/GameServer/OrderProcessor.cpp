#include "OrderProcessor.h"



OrderProcessor::OrderProcessor()
{
}


OrderProcessor::~OrderProcessor()
{
}

void OrderProcessor::addOrder(int id, sf::IpAddress ip, unsigned short port, C2SProtocol code, int messageID, float xDisplacement, float yDisplacement)
{
	bool add = true;
	for (auto it = orders.begin(); it != orders.end(); ++it) {
		if (xDisplacement < it->second.x + VEL || xDisplacement > it->second.x - VEL || yDisplacement <it->second.y + VEL || yDisplacement > it->second.y - VEL){
			if (it->first.id == id) {
				it->second.x += xDisplacement;
				it->second.y += yDisplacement;
				add = false;
				break;
			}
		}
	}
	if(add)
		orders.push_back(std::make_pair(BasicData{ id, port, ip, code, messageID }, sf::Vector2f{xDisplacement, yDisplacement}));
}

void OrderProcessor::Update(std::map<int, ClientProxy> clients)
{
//	while (!orders.empty()) {
//		if (clients.find(orders.front().first) == clients.end()) { //cliente no conocido
//			int newPlayerID = getValidID();
//			clients[newPlayerID].id = newPlayerID;
//			clients[newPlayerID].ip = ip;
//			clients[newPlayerID].port = port;
//
//			std::cout << port << " has connected" << std::endl;
//			sf::Packet pack;
//			pack.clear(); //When a new player enters we dont care what they say
//			pack << S2CProtocol::WELCOME;
//
//			pack << newPlayerID << clients[newPlayerID].pos.x << clients[newPlayerID].pos.y << static_cast<int>(clients.size() - 1); //El propio player
//			pack << static_cast<int>(clients.size());
//			for (std::pair<int, ClientProxy> prok : clients) { //Todos los clientes
//				pack << prok.second.id << prok.second.pos.x << prok.second.pos.y;
//			}
//			sf::UdpSocket::Status status = socket.send(pack, ip, port); //Se env�a al nuevo player
//			if (status != sf::Socket::Status::Done) {
//				std::cout << "error sending welcome" << std::endl;
//			}
//			pack.clear();
//			pack << S2CProtocol::NEWPLAYER;
//			pack << newPlayerID << clients[newPlayerID].pos.x << clients[newPlayerID].pos.y; //el nuevo player
//			for (std::pair<int, ClientProxy> prok : clients) { //Todos los clientes
//				socket.send(pack, prok.second.ip, prok.second.port);
//			}
//		}
//		else { //cliente conocido
//
//
//			C2SProtocol code;
//			pack >> code;
//			switch (code) {
//			case C2SProtocol::HELLO:
//				break;
//			case C2SProtocol::PONG:
//				std::cout << port << " pong" << std::endl;
//				break;
//			case C2SProtocol::MOVE:
//				int dir;
//				pack >> dir;
//
//				switch (dir) {
//				case 'l':
//					clients[id].pos.x -= VEL;
//					break;
//				case 'r':
//					clients[id].pos.x += VEL;
//					break;
//				case 'u':
//					clients[id].pos.y -= VEL;
//					break;
//				case 'd':
//					clients[id].pos.y += VEL;
//					break;
//				}
//
//				pack.clear();
//				pack << S2CProtocol::OKMOVE << id << clients[id].pos.x << clients[id].pos.y;
//				for (std::pair<int, ClientProxy> prok : clients) {
//					socket.send(pack, prok.second.ip, prok.second.port);
//				}
//				break;
//			}
//		}
//
//
//		processor.orders.pop();
//	}
}

bool OrderProcessor::canProcess()
{
	if (clock.getElapsedTime().asSeconds() > timeToProcess) {
		clock.restart();
		return true;
	}
	else
		return false;
}

int OrderProcessor::getValidID()
{
	return 0;
}
