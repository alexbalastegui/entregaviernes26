#pragma once
#include <iostream>
#include <SFML\Graphics.hpp>
#include <SFML\Network.hpp>
#include "NetworkManagerServer.h"
#include <thread>

void main() {
	NetworkManagerServer manager;
	std::thread ears = manager.launchHearingThread();
	std::thread heart = manager.launchHeartbeat();
	while (manager.serverIsRunning) {
		manager.MainLoop();
	}
}