#pragma once
#include <queue>
#include <deque>
#include <map>
#include <SFML/Network.hpp>
#include <ClientProxy.h>
#include <ProtocolsUDP.h>

struct BasicData {
	int id;
	unsigned short port;
	sf::IpAddress ip;
	C2SProtocol code;
	int messageID;
};

class OrderProcessor
{
public:
	OrderProcessor();
	~OrderProcessor();

	sf::Clock clock;

	std::deque<std::pair<BasicData, sf::Vector2f>> orders;
	void addOrder(int id, sf::IpAddress ip, unsigned short port, C2SProtocol code, int messageID, float xDisplacement, float yDisplacement);
	void Update(std::map<int, ClientProxy> clients);
	bool canProcess();
	int getValidID();
};

