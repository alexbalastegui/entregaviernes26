#include "NetworkManagerClient.h"

std::mutex moveListMutex;

float lerp(float a, float b, float f)
{
	return a+f*(b-a);
}



const sf::Color colorTable[8] = { sf::Color::Red, sf::Color::Green, sf::Color::Blue, sf::Color::Magenta, sf::Color::Yellow, sf::Color::Cyan, sf::Color::Black, sf::Color::White};

void NetworkManagerClient::initWindow()
{
	window = new sf::RenderWindow(sf::VideoMode(gameW, gameH), "Ball Game");
}

std::thread NetworkManagerClient::startHearing()
{
	return std::thread([=] {serverHearing(&socket, &movesList, &appIsRunning); });
}

void NetworkManagerClient::serverHearing(sf::UdpSocket * _socket, std::deque<Movement>* _movesList, bool *_appIsRunning)
{
	while (*_appIsRunning) {
		sf::Packet pack;
		sf::IpAddress ip;
		unsigned short port;
		if (_socket->receive(pack, ip, port)==sf::Socket::Status::Done) {
			if (ip == serverIP && port == serverPort) {//La información la ha enviado el server
				std::string msg;
				S2CProtocol code;
				pack >> code;
				switch (code) {
				case S2CProtocol::WELCOME:
					//std::cout << "Server says welcome" << std::endl;
					//pack >> msg;
					//std::cout << msg << std::endl;
					//Se Recibe la informacion inicial.
					threadAlive = false;
					//std::cout << "Hearing thread: " << threadAlive << std::endl;

					pack >> me->id >> me->pos.x >> me->pos.y >> rivalAmount;
					enqueuer.id = me->id;

					me->color = colorTable[me->id];
					me->drawable->setFillColor(me->color);
					me->drawable->setPosition(me->pos.x, me->pos.y);

					int playerAmount;
					pack >> playerAmount;
					for (int i = 0; i < playerAmount; ++i) {
						int newID;
						float posX, posY;
						pack >> newID >> posX >> posY;
						players[newID].id = newID;
						players[newID].pos.x = players[newID].drawablePos.x = posX;
						players[newID].pos.y = players[newID].drawablePos.y = posY;
						players[newID].color = colorTable[newID];
						players[newID].drawable = new sf::CircleShape(CIRCLERAD);
						players[newID].drawable->setPosition(posX, posY);
						players[newID].drawable->setFillColor(colorTable[newID]);
					}

					pack.clear();
					//Enviar acknowledge de mi conexion.
					//pack << C2SProtocol::ACK;
					//_socket->send(pack, serverIP, serverPort);
					break;
				case S2CProtocol::PING:
					std::cout << "ping" << std::endl;
					pack.clear();
					pack << me->id << C2SProtocol::PONG;
					if (appIsRunning)
						socket.send(pack, serverIP, serverPort);
					break;
				case S2CProtocol::OKMOVE:
					int playerID, messageID;
					float newX, newY;
					pack >> playerID >> newX >> newY >> messageID;
					std::cout << "Received OKMOVE" << std::endl;
						if (playerID == me->id) {
							moveListMutex.lock();
							for (auto it = _movesList->begin(); it != _movesList->end(); ++it) {
								if (messageID == it->id) { //Check movement ID

									if (it->abs.x == players[playerID].pos.x && it->abs.y == players[playerID].pos.y) {

									}
									else {
										players[playerID].pos.x = it->abs.x;
										players[playerID].pos.y = it->abs.y;
										std::cout << "Correcting movement" << std::endl;
									}

									/*players[playerID].pos.x = newX;
									players[playerID].pos.y = newY;
									players[playerID].drawable->setPosition(players[playerID].pos.x, players[playerID].pos.y);
									
									for (auto it2 = it; it2 != _movesList->end(); ++it2) {
										players[playerID].pos.x += it2->delta.x;
										players[playerID].pos.y += it2->delta.y;
									}*/

									_movesList->erase(_movesList->begin(), it);
									break;
								}
							}
							moveListMutex.unlock();
						}
						else {
							players[playerID].pos.x = newX;
							players[playerID].pos.y = newY;
						}
					//std::cout << "X: " << newX << " Y: " << newY << " ID: " << playerID << std::endl;
					break;
				case S2CProtocol::NEWPLAYER: {
					int newPlayerID;
					float newPlayerX, newPlayerY;
					pack >> newPlayerID >> newPlayerX >> newPlayerY;
					players[newPlayerID].id = newPlayerID;
					players[newPlayerID].pos.x = players[newPlayerID].drawablePos.x = newPlayerX;
					players[newPlayerID].pos.y = players[newPlayerID].drawablePos.y = newPlayerY;
					players[newPlayerID].color = colorTable[newPlayerID];
					players[newPlayerID].drawable = new sf::CircleShape(CIRCLERAD);
					players[newPlayerID].drawable->setPosition(newPlayerX, newPlayerY);
					players[newPlayerID].drawable->setFillColor(colorTable[newPlayerID]);
					pack.clear();
					pack << me->id << C2SProtocol::OKNEWPLAYER;
					if (appIsRunning)
						socket.send(pack, serverIP, serverPort);
					//std::cout << "Received new player" << std::endl;
				}
					break;
				case S2CProtocol::DISCONNECTOK:
					threadAlive = false;
					*_appIsRunning = false;
					break;
				}
				pack.clear();
			}
		}
	}

}

void NetworkManagerClient::criticalThread(sf::UdpSocket* _socket, sf::Packet pack, bool* _threadAlive)
{
	while (*_threadAlive) {
		//std::cout << "Critical thread alive" << std::endl;
		if (criticalClock.getElapsedTime().asSeconds() > helloPollingTime) {
			//std::cout << "Critical thread: " << *_threadAlive << std::endl;
			if(appIsRunning)
				socket.send(pack, serverIP, serverPort);
			criticalClock.restart();
		}
	}
}

std::thread NetworkManagerClient::launchCriticalThread(sf::UdpSocket * _socket, sf::Packet pack, bool * _threadAlive)
{
	return std::thread([=] {criticalThread(_socket, pack, _threadAlive); });
}

void NetworkManagerClient::MainLoop()
{
	while (window->isOpen()) {
		//event handling
		sf::Event evnt;
		while (window->pollEvent(evnt)) {
			switch (evnt.type) {
			case sf::Event::EventType::Closed:

				sf::Packet pack;
				pack << me->id << C2SProtocol::DISCONNECT;

				threadAlive = true;
				std::thread closeMsg = launchCriticalThread(&socket, pack, &threadAlive);
				closeMsg.detach();
				break;
			}
		}

		//update


		if ( window->hasFocus() && appIsRunning) {
			enqueuer.Update();

			if ((sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::Right) ||
				sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::Down))) {
				if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left)) {
					sf::Packet pack;
					enqueuer.enqueueOrder(-1*VEL, 0, players[me->id].pos.x, players[me->id].pos.y);
					players[me->id].pos.x += (-1 * VEL);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right)) {
					enqueuer.enqueueOrder(1 * VEL, 0, players[me->id].pos.x, players[me->id].pos.y);
					players[me->id].pos.x += (1 * VEL);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up)) {
					enqueuer.enqueueOrder(0, -1 * VEL, players[me->id].pos.x, players[me->id].pos.y);
					players[me->id].pos.y += (-1 * VEL);
				}
				else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down)) {
					enqueuer.enqueueOrder(0, 1 * VEL, players[me->id].pos.x, players[me->id].pos.y);
					players[me->id].pos.y += (1 * VEL);
				}
				else {
					enqueuer.enqueueOrder(0, 1 * VEL, players[me->id].pos.x, players[me->id].pos.y);
				}
				me->drawable->setPosition(me->pos);
				players[me->id].drawable->setPosition(players[me->id].pos);
			}
		}		

		if (!appIsRunning)
			window->close();

		//draw
		
		for (auto it = players.begin(); it != players.end(); ++it) {
			if (it->second.id != me->id) {
				it->second.drawablePos.x = lerp(it->second.drawablePos.x, it->second.pos.x, playerInterpAlpha);
				it->second.drawablePos.y = lerp(it->second.drawablePos.y, it->second.pos.y, playerInterpAlpha);

				it->second.drawable->setPosition(it->second.drawablePos);
				//std::cout << it->second.drawablePos.x << " - " << it->second.pos.x << std::endl;
			}
			window->draw(*(it->second.drawable));
		}

		window->display();
		window->clear(sf::Color(191, 252, 220));
	}
}

NetworkManagerClient::NetworkManagerClient()
{
	me = new Player;
	enqueuer.ip = serverIP;
	enqueuer.port = serverPort;
	enqueuer.socket = &socket;


	appIsRunning = true;
	sf::Packet pack;
	pack << me->id << C2SProtocol::HELLO;
	threadAlive = true;
	std::thread helloThread = launchCriticalThread(&socket, pack, &threadAlive);
	helloThread.detach();

	initWindow();
	me->drawable = new sf::CircleShape(CIRCLERAD);
	me->drawable->setPosition(me->pos.x, me->pos.y);

	enqueuer.movesList = &movesList;
}

void NetworkManagerClient::Connected(sf::Packet &pack)
{
	int ID;
	sf::Vector2i position;
	int playerAmmount;
	pack >> ID >> position.x >> position.y >> playerAmmount;
	//std::cout << "ID: " << ID << std::endl << "Position: " << position.x << " " << position.y << std::endl << "PlayersAmmount: " << playerAmmount<<std::endl;
}

NetworkManagerClient::~NetworkManagerClient()
{
}
