#pragma once
#include <queue>
#include <thread>
#include <SFML/Network.hpp>
#include <iostream>
#include <constants.h>
#include <ProtocolsUDP.h>
#include <ClientProxy.h>

class OrderEnqueuer
{
public:
	OrderEnqueuer();
	~OrderEnqueuer();
	
	sf::UdpSocket* socket;
	unsigned short port;
	sf::IpAddress ip;
	int id=-1;
	int messageID = 0;


	std::deque<Movement>* movesList;
	std::queue<std::pair<sf::Vector2f, sf::Vector2f>> orders;
	sf::Clock enqueueClock;

	void enqueueOrder(float xDisplacement, float yDisplacement, float xAbsolute, float yAbsolute);
	void processOrder();
	void Update();
};

