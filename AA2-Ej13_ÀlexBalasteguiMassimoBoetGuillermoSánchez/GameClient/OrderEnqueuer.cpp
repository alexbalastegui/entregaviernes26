#include "OrderEnqueuer.h"



OrderEnqueuer::OrderEnqueuer()
{
}


OrderEnqueuer::~OrderEnqueuer()
{
}

void OrderEnqueuer::enqueueOrder(float xDisplacement, float yDisplacement, float xAbsolute, float yAbsolute)
{
	orders.push( std::make_pair(sf::Vector2f(xDisplacement, yDisplacement), sf::Vector2f(xAbsolute,yAbsolute)) );
}

void OrderEnqueuer::processOrder()
{
	bool send = !orders.empty();
	float accumX, accumY;
	float lastAbsoluteX, lastAbsoluteY;
	accumX = accumY = 0;
	float amount=orders.size();
	while (!orders.empty()) {
		accumX += orders.front().first.x;
		accumY += orders.front().first.y;
		lastAbsoluteX = orders.front().second.x;
		lastAbsoluteY = orders.front().second.y;
		orders.pop();
	}
	if (!orders.empty()) {
		accumX /= amount;
		accumY /= amount;
	}

	if (send) {
		sf::Packet pack;
		Movement move;
		move.id = messageID++;
		move.delta.x = accumX;
		move.delta.y = accumY;
		pack << id << C2SProtocol::MOVE << move;
		std::cout << "X: " << lastAbsoluteX << " Y: " << lastAbsoluteY << std::endl;
		socket->send(pack, ip, port);
		movesList->push_back({ messageID, {accumX, accumY}, {lastAbsoluteX, lastAbsoluteY} });
	}
}

void OrderEnqueuer::Update()
{
	if (enqueueClock.getElapsedTime().asSeconds() > orderAcceptTime) {
		if(id!=-1)
			processOrder();
		enqueueClock.restart();
	}
}
