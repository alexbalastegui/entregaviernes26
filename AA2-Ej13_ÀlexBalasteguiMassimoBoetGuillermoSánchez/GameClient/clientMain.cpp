#pragma once
#include <ClientProxy.h>
#include <SFML\Network.hpp>
#include <iostream>
#include "NetworkManagerClient.h"
#include <thread>

void main() {
	NetworkManagerClient manager;
	std::thread ears = manager.startHearing();
	ears.detach();
	while(manager.appIsRunning)
		manager.MainLoop();
}