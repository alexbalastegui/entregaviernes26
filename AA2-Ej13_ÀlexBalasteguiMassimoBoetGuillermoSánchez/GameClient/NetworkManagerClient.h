#pragma once
#include <SFML/Network.hpp>
#include <SFML/Graphics.hpp>
#include <thread>
#include <iostream>
#include <ProtocolsUDP.h>
#include <map>
#include "OrderEnqueuer.h"
#include <math.h>
#include <deque>
#include <mutex>

struct Player {

	int id = -1;
	sf::Vector2f pos;
	sf::Vector2f drawablePos;
	sf::CircleShape* drawable;
	sf::Color color;
};

class NetworkManagerClient
{
public:
	sf::UdpSocket socket;
	sf::IpAddress serverIP="localhost";
	unsigned short serverPort=50000;
	bool appIsRunning = true;
	
	sf::RenderWindow* window=nullptr;
	void initWindow();
	Player* me;
	std::map<int, Player> players;
	int rivalAmount = -1;

	OrderEnqueuer enqueuer;
	std::deque<Movement> movesList;

	std::thread startHearing();
	void serverHearing(sf::UdpSocket* _socket, std::deque<Movement>* _movesList, bool *_appIsRunning);

	sf::Clock criticalClock;
	bool threadAlive = false;
	void criticalThread(sf::UdpSocket* _socket, sf::Packet pack, bool* _threadAlive);
	std::thread launchCriticalThread(sf::UdpSocket* _socket, sf::Packet pack, bool* _threadAlive);

	void MainLoop();
	void NetworkManagerClient::Connected(sf::Packet &pack);


	NetworkManagerClient();
	~NetworkManagerClient();
};

